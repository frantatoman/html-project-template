
 //Gruntfile
    module.exports = function(grunt) {

    //Initializing the configuration object
      grunt.initConfig({

        pkg: grunt.file.readJSON('package.json'),

        // Task configuration
        concat: {
          options: {
            separator: ';',
          },
          js_frontend: {
            src: [
              './bower_components/jquery/dist/jquery.js',
              './bower_components/bootstrap/dist/js/bootstrap.js',
              './dev/js/frontend.js'
            ],
            dest: './public/js/main.js',
          }
        },

        less:{
          development: {
            options: {
              compress: true,  //minifying the result
            },
            files: {
              //compiling base.less into screen.css
              "./public/css/screen.css":"./dev/less/base.less",
            }
          }
        },
        uglify:{
          options: {
            mangle: false  // Use if you want the names of your functions and variables unchanged
          },
          frontend: {
            files: {
              './public/js/main.js': './public/js/main.js',
            }
          },
        },

        watch:{

            js_frontend: {
              files: [
                //watched files
                './bower_components/jquery/dist/jquery.js',
                './bower_components/bootstrap/dist/js/bootstrap.js',
                './dev/js/frontend.js'
                ],   
              tasks: ['concat:js_frontend','uglify:frontend'],     //tasks to run
              options: {
                livereload: true                        //reloads the browser
              }
            },
            
            less: {
              files: ['./dev/less/*.less'],  //watched files
              tasks: ['less'],                          //tasks to run
              options: {
                livereload: true                        //reloads the browser
              }
            },

        }
      });

    // Plugin loading

     grunt.loadNpmTasks('grunt-contrib-concat');
     grunt.loadNpmTasks('grunt-contrib-watch');
     grunt.loadNpmTasks('grunt-contrib-less');
     grunt.loadNpmTasks('grunt-contrib-uglify');
     
     // Task definition
     grunt.registerTask('default', ['watch']);

  };